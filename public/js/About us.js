(self["webpackChunk"] = self["webpackChunk"] || []).push([["About us"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/components/AboutUs.vue?vue&type=script&lang=js":
/*!*************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/components/AboutUs.vue?vue&type=script&lang=js ***!
  \*************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var vue3_autocounter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue3-autocounter */ "./node_modules/vue3-autocounter/dist/vue3-autocounter.esm.js");

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  components: {
    'vue3-autocounter': vue3_autocounter__WEBPACK_IMPORTED_MODULE_0__.default
  },
  mounted: function mounted() {
    window.scrollTo(0, 0);
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/components/AboutUs.vue?vue&type=template&id=19b32af0":
/*!*****************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/components/AboutUs.vue?vue&type=template&id=19b32af0 ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render)
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _assets_aboutuslogo_png__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../assets/aboutuslogo.png */ "./resources/js/assets/aboutuslogo.png");
/* harmony import */ var _assets_vision_png__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../assets/vision.png */ "./resources/js/assets/vision.png");
/* harmony import */ var _assets_aboutusmission_png__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../assets/aboutusmission.png */ "./resources/js/assets/aboutusmission.png");
/* harmony import */ var _assets_objectives_png__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../assets/objectives.png */ "./resources/js/assets/objectives.png");





var _hoisted_1 = {
  "class": "aboutus",
  style: {
    "margin-top": "auto"
  }
};

var _hoisted_2 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("span", {
  "class": "aboutusback"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("p", {
  "class": "headings",
  style: {
    "margin": "auto"
  }
}, "Who We Are"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("p", {
  style: {
    "color": "#707070",
    "font-size": "12px"
  }
}, "About Us / Who we are")], -1
/* HOISTED */
);

var _hoisted_3 = {
  "class": "container",
  style: {
    "margin-top": "2rem"
  }
};

var _hoisted_4 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", {
  "class": "aboutrows"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", {
  "class": "aboutimagecol"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("img", {
  src: _assets_aboutuslogo_png__WEBPACK_IMPORTED_MODULE_1__.default,
  alt: "",
  srcset: "",
  "class": "aboutimage"
})]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", {
  "class": "abouttextcol"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("p", {
  "class": "headings",
  style: {
    "font-size": "24px",
    "width": "100%"
  }
}, "Glimpse of MSeGS"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("img", {
  src: _assets_aboutuslogo_png__WEBPACK_IMPORTED_MODULE_1__.default,
  alt: "",
  srcset: "",
  "class": "aboutimagesmobile"
}), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("p", {
  "class": "aboutcontents"
}, " Mizoram State e-Governance Society (MSeGS) is an autonomous society under Government of Mizoram and Non-Profit Organization setup in 2005 with Chief Secretary to the Government of Mizoram as its Chairman. MSeGS is a nodal agency for implementing various e-Governance projects in the state of Mizoram. MSeGS does not enter into any revenue-generating engagements with the private sector. All the engagements executed by MSeGS so far have been awarded on a nomination basis only. As a policy and practice, MSeGS does not participate in any bidding process. The general practice adopted by all the departments/organizations is to get specific internal approvals by the competent authorities. ")])], -1
/* HOISTED */
);

var _hoisted_5 = {
  "class": "landing4",
  style: {
    "background-color": "white",
    "margin-bottom": "2rem"
  }
};

var _hoisted_6 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("p", {
  "class": "headings",
  style: {
    "text-align": "center",
    "margin": "auto",
    "padding-top": "1rem",
    "padding-bottom": "1rem",
    "font-size": "24px"
  }
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("span", {
  style: {
    "border-bottom": "3px solid #dc3d32"
  }
}, " OUR PROFILE ")], -1
/* HOISTED */
);

var _hoisted_7 = {
  "class": "ourprofilerows"
};
var _hoisted_8 = {
  "class": "ourprofileitems"
};
var _hoisted_9 = {
  id: "ourprofilenumber"
};

var _hoisted_10 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("p", {
  id: "ourprofilecontent"
}, "years of experience", -1
/* HOISTED */
);

var _hoisted_11 = {
  "class": "ourprofileitems"
};
var _hoisted_12 = {
  id: "ourprofilenumber"
};

var _hoisted_13 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("p", {
  id: "ourprofilecontent"
}, "Awards", -1
/* HOISTED */
);

var _hoisted_14 = {
  "class": "ourprofileitems"
};
var _hoisted_15 = {
  id: "ourprofilenumber"
};

var _hoisted_16 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("p", {
  id: "ourprofilecontent"
}, "Employees", -1
/* HOISTED */
);

var _hoisted_17 = {
  "class": "ourprofileitems"
};
var _hoisted_18 = {
  id: "ourprofilenumber"
};

var _hoisted_19 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("p", {
  id: "ourprofilecontent"
}, "Projects launched", -1
/* HOISTED */
);

var _hoisted_20 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", {
  "class": "aboutrows"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", {
  "class": "aboutimagecol"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("img", {
  src: _assets_vision_png__WEBPACK_IMPORTED_MODULE_2__.default,
  alt: "",
  srcset: "",
  "class": "aboutimages"
})]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", {
  "class": "abouttextcol"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("p", {
  "class": "headings",
  style: {
    "font-size": "24px"
  }
}, "Our Vision"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("img", {
  src: _assets_vision_png__WEBPACK_IMPORTED_MODULE_2__.default,
  alt: "",
  srcset: "",
  "class": "aboutimagesmobile"
}), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("p", {
  "class": "aboutcontents"
}, " To help the state government for transforming governance into citizen centric services and provide smart service delivery system. ")])], -1
/* HOISTED */
);

var _hoisted_21 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", {
  "class": "aboutrows"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", {
  "class": "abouttextcol"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("p", {
  "class": "headings",
  style: {
    "font-size": "24px"
  }
}, "Our Mission"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("img", {
  src: _assets_aboutusmission_png__WEBPACK_IMPORTED_MODULE_3__.default,
  alt: "",
  srcset: "",
  "class": "aboutimagesmobile"
}), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("p", {
  "class": "aboutcontents"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("ul", {
  "class": "aboutlists"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("li", null, " Creating enterprise architecture and provide smart solution. "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("li", null, "Assists state government in e-governance to improve public service delivery mechanism. "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("li", null, "Skill development to unemployed youth in emerging technologies for employability in government and private sector.")])])]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", {
  "class": "aboutimagecol"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("img", {
  src: _assets_aboutusmission_png__WEBPACK_IMPORTED_MODULE_3__.default,
  alt: "",
  srcset: "",
  "class": "aboutimages"
})])], -1
/* HOISTED */
);

var _hoisted_22 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", {
  "class": "aboutrows"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", {
  "class": "aboutimagecol"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("img", {
  src: _assets_objectives_png__WEBPACK_IMPORTED_MODULE_4__.default,
  alt: "",
  srcset: "",
  "class": "aboutimages"
})]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", {
  "class": "abouttextcol"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("p", {
  "class": "headings",
  style: {
    "font-size": "24px"
  }
}, "Our Objectives"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("img", {
  src: _assets_objectives_png__WEBPACK_IMPORTED_MODULE_4__.default,
  alt: "",
  srcset: "",
  "class": "aboutimagesmobile"
}), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("p", {
  "class": "aboutcontents"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("ul", {
  "class": "aboutlists"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("li", null, "Implementation, monitoring and evaluation of various e-governance projects in the state."), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("li", null, "Provide citizen facilitation centre or common service centre in every village to delivery various services to the citizen."), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("li", null, "Assist the state government to collect revenue through online and transfer the revenue collected to the concerned departments."), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("li", null, "Improve government service delivery system with emerging technologies."), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("li", null, "Provide share infrastructure to all government departments for network, data centre and public service delivery centres reducing the total cost of ownership. "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("li", null, "Help the state government for implementing Right to Public Services (RTPS) to reduce corruption and to increase transparency and public accountability."), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("li", null, "Provide technology and management consultancy services to the state government for implementation of e-governance in various departments."), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("li", null, "Promote local IT entrepreneurs by enhancement of quality management system and business continuity to sustain in the digital world. "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("li", null, "Carrying out impact assessment and e-readiness measurement of e-governance projects of the state government."), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("li", null, "Manpower outsourcing to state government departments by recruiting, deployment and HR management of specialised resources."), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("li", null, "Assist the state government to setup cyber security lab enhancing safe and secure cyberspace in the state. ")])])])], -1
/* HOISTED */
);

function render(_ctx, _cache, $props, $setup, $data, $options) {
  var _component_vue3_autocounter = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("vue3-autocounter");

  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)("div", _hoisted_1, [_hoisted_2, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_3, [_hoisted_4, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_5, [_hoisted_6, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_7, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_8, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("p", _hoisted_9, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_vue3_autocounter, {
    ref: "counter",
    startAmount: 0,
    endAmount: 15,
    duration: 3,
    prefix: "",
    suffix: "+",
    separator: "",
    decimalSeparator: " ",
    decimals: 0,
    autoinit: true
  }, null, 512
  /* NEED_PATCH */
  )]), _hoisted_10]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_11, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("p", _hoisted_12, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_vue3_autocounter, {
    ref: "counter",
    startAmount: 0,
    endAmount: 6,
    duration: 3,
    prefix: "0",
    suffix: "+",
    separator: "",
    decimalSeparator: " ",
    decimals: 0,
    autoinit: true
  }, null, 512
  /* NEED_PATCH */
  )]), _hoisted_13]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_14, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("p", _hoisted_15, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_vue3_autocounter, {
    ref: "counter",
    startAmount: 0,
    endAmount: 30,
    duration: 3,
    prefix: "",
    suffix: "+",
    separator: "",
    decimalSeparator: " ",
    decimals: 0,
    autoinit: true
  }, null, 512
  /* NEED_PATCH */
  )]), _hoisted_16]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("div", _hoisted_17, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)("p", _hoisted_18, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_vue3_autocounter, {
    ref: "counter",
    startAmount: 0,
    endAmount: 500,
    duration: 3,
    prefix: "",
    suffix: "+",
    separator: "",
    decimalSeparator: " ",
    decimals: 0,
    autoinit: true
  }, null, 512
  /* NEED_PATCH */
  )]), _hoisted_19])])]), _hoisted_20, _hoisted_21, _hoisted_22])]);
}

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/components/AboutUs.vue?vue&type=style&index=0&id=19b32af0&lang=scss":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/components/AboutUs.vue?vue&type=style&index=0&id=19b32af0&lang=scss ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".aboutimagesmobile {\n  display: none;\n}\n.aboutusback {\n  background-color: #ececec;\n  background-color: #ececec;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  padding: 3rem 0 3rem 0;\n}\n.aboutrows {\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  margin-bottom: 3rem;\n}\n.aboutimagecol {\n  width: 50%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n.aboutimage {\n  width: 60%;\n  height: auto;\n}\n.aboutimages {\n  width: 80%;\n  height: auto;\n  padding: 1rem;\n}\n.abouttextcol {\n  width: 50%;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n}\n.aboutcontents {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  font-family: Poppins;\n  font-size: 16px;\n  font-weight: normal;\n  font-stretch: normal;\n  font-style: normal;\n  line-height: 1.88;\n  letter-spacing: normal;\n  text-align: left;\n  color: #393939;\n}\n.aboutlists {\n  margin-left: -1rem;\n}\n@media screen and (max-width: 800px) {\ndiv.aboutrows:nth-child(1) > div:nth-child(2) > img:nth-child(2) {\n    width: 60vw;\n}\n.aboutimage {\n    display: none;\n}\n.aboutimagesmobile {\n    display: block;\n    width: 80vw;\n}\n.aboutimages {\n    display: none;\n    width: 100%;\n    height: auto;\n}\n.abouttextcol {\n    display: block;\n    width: 100%;\n    align-items: center;\n}\ndiv.abouttextcol:nth-child(n) > p:nth-child(n) {\n    min-width: 90%;\n}\n.aboutrows {\n    display: flex;\n    flex-direction: column;\n    justify-content: space-between;\n    margin-bottom: 3rem;\n}\n.aboutimagecol {\n    width: 80%;\n    display: flex;\n    margin: auto;\n    justify-content: center;\n    align-items: center;\n}\n.aboutimage {\n    width: 60%;\n    height: auto;\n}\n.abouttextcol {\n    width: 100%;\n    align-items: center;\n}\n.aboutcontents {\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    font-family: Poppins;\n    font-size: 16px;\n    font-weight: normal;\n    font-stretch: normal;\n    font-style: normal;\n    line-height: 1.88;\n    letter-spacing: normal;\n    text-align: left;\n    color: #393939;\n}\n.aboutlists {\n    margin-left: -1rem;\n}\n}\n", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./resources/js/assets/aboutuslogo.png":
/*!*********************************************!*\
  !*** ./resources/js/assets/aboutuslogo.png ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("/images/aboutuslogo.png?01412b5293701e7cebec820e3d51ff94");

/***/ }),

/***/ "./resources/js/assets/aboutusmission.png":
/*!************************************************!*\
  !*** ./resources/js/assets/aboutusmission.png ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("/images/aboutusmission.png?a07f38b0283ae9c3b19cacc3daad00c8");

/***/ }),

/***/ "./resources/js/assets/objectives.png":
/*!********************************************!*\
  !*** ./resources/js/assets/objectives.png ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("/images/objectives.png?365acf89c76028141288a90c115f740f");

/***/ }),

/***/ "./resources/js/assets/vision.png":
/*!****************************************!*\
  !*** ./resources/js/assets/vision.png ***!
  \****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("/images/vision.png?692ef5dd5283567513e8aee0cb2742aa");

/***/ }),

/***/ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/components/AboutUs.vue?vue&type=style&index=0&id=19b32af0&lang=scss":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/components/AboutUs.vue?vue&type=style&index=0&id=19b32af0&lang=scss ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_AboutUs_vue_vue_type_style_index_0_id_19b32af0_lang_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!../../../node_modules/vue-loader/dist/stylePostLoader.js!../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./AboutUs.vue?vue&type=style&index=0&id=19b32af0&lang=scss */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/components/AboutUs.vue?vue&type=style&index=0&id=19b32af0&lang=scss");

            

var options = {};

options.insert = "head";
options.singleton = false;

var update = _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_AboutUs_vue_vue_type_style_index_0_id_19b32af0_lang_scss__WEBPACK_IMPORTED_MODULE_1__.default, options);



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_AboutUs_vue_vue_type_style_index_0_id_19b32af0_lang_scss__WEBPACK_IMPORTED_MODULE_1__.default.locals || {});

/***/ }),

/***/ "./resources/js/components/AboutUs.vue":
/*!*********************************************!*\
  !*** ./resources/js/components/AboutUs.vue ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _AboutUs_vue_vue_type_template_id_19b32af0__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AboutUs.vue?vue&type=template&id=19b32af0 */ "./resources/js/components/AboutUs.vue?vue&type=template&id=19b32af0");
/* harmony import */ var _AboutUs_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AboutUs.vue?vue&type=script&lang=js */ "./resources/js/components/AboutUs.vue?vue&type=script&lang=js");
/* harmony import */ var _AboutUs_vue_vue_type_style_index_0_id_19b32af0_lang_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./AboutUs.vue?vue&type=style&index=0&id=19b32af0&lang=scss */ "./resources/js/components/AboutUs.vue?vue&type=style&index=0&id=19b32af0&lang=scss");




;
_AboutUs_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__.default.render = _AboutUs_vue_vue_type_template_id_19b32af0__WEBPACK_IMPORTED_MODULE_0__.render
/* hot reload */
if (false) {}

_AboutUs_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__.default.__file = "resources/js/components/AboutUs.vue"

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_AboutUs_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__.default);

/***/ }),

/***/ "./resources/js/components/AboutUs.vue?vue&type=script&lang=js":
/*!*********************************************************************!*\
  !*** ./resources/js/components/AboutUs.vue?vue&type=script&lang=js ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_AboutUs_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__.default)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_AboutUs_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./AboutUs.vue?vue&type=script&lang=js */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/components/AboutUs.vue?vue&type=script&lang=js");
 

/***/ }),

/***/ "./resources/js/components/AboutUs.vue?vue&type=template&id=19b32af0":
/*!***************************************************************************!*\
  !*** ./resources/js/components/AboutUs.vue?vue&type=template&id=19b32af0 ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_AboutUs_vue_vue_type_template_id_19b32af0__WEBPACK_IMPORTED_MODULE_0__.render)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_AboutUs_vue_vue_type_template_id_19b32af0__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./AboutUs.vue?vue&type=template&id=19b32af0 */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/components/AboutUs.vue?vue&type=template&id=19b32af0");


/***/ }),

/***/ "./resources/js/components/AboutUs.vue?vue&type=style&index=0&id=19b32af0&lang=scss":
/*!******************************************************************************************!*\
  !*** ./resources/js/components/AboutUs.vue?vue&type=style&index=0&id=19b32af0&lang=scss ***!
  \******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_AboutUs_vue_vue_type_style_index_0_id_19b32af0_lang_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader/dist/cjs.js!../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!../../../node_modules/vue-loader/dist/stylePostLoader.js!../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./AboutUs.vue?vue&type=style&index=0&id=19b32af0&lang=scss */ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/components/AboutUs.vue?vue&type=style&index=0&id=19b32af0&lang=scss");


/***/ })

}]);